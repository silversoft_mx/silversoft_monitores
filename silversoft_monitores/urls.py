from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from res.views import RazaCreateView, RazaListView, RazaDetailView

urlpatterns = [
	url(r'^inicio/$', 'planta.views.inicio', name='inicio'),
	url(r'^$', 'planta.views.home', name='home'),
	url(r'^entrar/$', 'planta.views.entrar', name='entrar'),
	url(r'^salir/$', 'planta.views.salir', name='salir'),
	url(r'^configuracion/$', 'planta.views.configuracion', name='configuracion'),

	url(r'^res/registrar/$', 'res.views.res_registrar', name='res_registrar'),
	url(r'^res/([0-9]+)/$', 'res.views.res_ver', name='res_ver'),
	url(r'^res/alerta/([0-9]+)/$', 'res.views.res_alerta', name='res_ver'),

	url(r'^raza/$', RazaListView.as_view(), name='raza'),
	url(r'^raza/(?P<pk>[0-9]+)/$', RazaDetailView.as_view(), name='raza_detail'),
	url(r'^raza/registrar/$', RazaCreateView.as_view(), name='raza_registrar'),



    url(r'^admin/', include(admin.site.urls)),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
