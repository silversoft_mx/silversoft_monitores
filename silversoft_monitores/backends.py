from django.contrib.auth.models import check_password, User

class EmailAuthBackend(object):
	def authenticate(self, username=None, password=None):
		try:
			user = User.objects.get(email=username)
			if user.check_password(password) and user.is_active == True:
				return user
		except User.DoesNotExist:
			return None
		return None

	def get_user(self, user_id):
		try:
			return User.objects.get(pk=user_id)
		except User.DoesNotExist:
			return None
		return None