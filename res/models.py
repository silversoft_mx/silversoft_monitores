from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User


from planta.models import Ciudad

class Raza(models.Model):
	nombre = models.CharField(max_length=100)

	def __unicode__(self):
		return u'%s' % self.nombre
		
	def get_absolute_url(self):
		return reverse('raza_detail', kwargs={'pk': self.pk})

class Proveedor(models.Model):
	nombre = models.CharField(max_length=100)
	telefono = models.CharField(max_length=15, blank=True)
	ciudad = models.ForeignKey(Ciudad, blank=True)
	descripcion = models.TextField(blank=True)

class Receptor(models.Model):
	id = models.IntegerField(unique=True, primary_key=True)
	nombre = models.CharField(max_length=100)

class Corral(models.Model):
	nombre = models.CharField(max_length=100)

class Res(models.Model):
	proveedor = models.ForeignKey(Proveedor, null=True, blank=True)
	fecha_ingreso = models.DateField(auto_now_add=True)
	raza = models.ForeignKey(Raza, null=True, blank=True)
	edad_ingreso = models.IntegerField(null=True, blank=True)
	SEXO = (
		('M', 'Macho'),
		('H', 'Hembra')
	)
	sexo = models.CharField(max_length=1, choices=SEXO, default='M')
	fecha_salida = models.DateField(null=True, blank=True)
	muerto = models.BooleanField(default=False)
	precio_compra = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
	precio_venta = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
	arete = models.IntegerField(null=True, unique=True, blank=True)
	arete_siniiga = models.CharField(max_length=100, blank=True)
	receptor = models.IntegerField(null=True, blank=True)
	corral = models.ForeignKey(Corral, null=True, blank=True)

	def medidas(self):
		return Medida.objects.filter(res=self.pk).order_by('-fecha')
	def signos_vitales(self):
		return Signo_Vital.objects.filter(res=self.pk).order_by('-fecha')
	def ultima_medida(self):
		medida = { 'peso': '', 'altura': '' }
		m = Medida.objects.filter(res=self.pk).order_by('-fecha').first()
		if m is not None:
			medida['peso'] = m.peso
			medida['altura'] = m.altura
		return medida
	def ultimo_signo_vital(self):
		signos_vitales = { 'temperatura': '', 'frecuencia_respiratoria': '', 'frecuencia_cardiaca': '' }
		sg = Signo_Vital.objects.filter(res=self.pk).order_by('-fecha').first()
		if sg is not None:
			signos_vitales['temperatura'] = sg.temperatura
			signos_vitales['frecuencia_respiratoria'] = sg.frecuencia_respiratoria
			signos_vitales['frecuencia_cardiaca'] = sg.frecuencia_cardiaca
		return signos_vitales

	def estado(self):
		est = Alerta.objects.filter(res=self.pk).order_by('-id')
		if not est.exists():
			return ''
		return est.first()

	def alertas(self):
		return Alerta.objects.filter(res=self.pk).order_by('-id')

	def enferma(self):
		est = Alerta.objects.exclude(estado='finalizado').filter(res=self.pk).order_by('-id')
		if est.exists():
			return True
		return False

class Medida(models.Model):
	res = models.ForeignKey(Res)
	edad = models.CharField(max_length=5, blank=True)
	peso = models.DecimalField(max_digits=6, decimal_places=2)
	altura = models.DecimalField(max_digits=6, decimal_places=2)
	fecha = models.DateTimeField(auto_now_add=True)

class Signo_Vital(models.Model):
	res = models.ForeignKey(Res)
	temperatura = models.DecimalField(max_digits=6, decimal_places=2)
	frecuencia_cardiaca = models.DecimalField(max_digits=6, decimal_places=2)
	frecuencia_respiratoria = models.DecimalField(max_digits=6, decimal_places=2)
	fecha = models.DateTimeField(auto_now_add=True)

class Enfermedad(models.Model):
	nombre = models.CharField(max_length=100)
	tipo = models.CharField(max_length=100)

class Enfermedad_Transaccion(models.Model):
	veterinario = models.ForeignKey(User)
	res = models.ForeignKey(Res)
	enfermedad = models.ForeignKey(Enfermedad)
	problema = models.TextField()

class Alerta(models.Model):
	res = models.ForeignKey(Res)
	ESTADO = (
		('checar', 'CHECAR'),
		('revisado', 'CHECADO'),
		('finalizado', 'FINALIZADO')
	)
	estado = models.CharField(max_length=20, choices=ESTADO, default='checar')
	fecha = models.DateTimeField(auto_now_add=True)
	updatedAt = models.DateTimeField(auto_now=True)
	comentario = models.TextField(blank=True)