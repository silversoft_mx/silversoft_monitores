from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from .models import Res, Raza
from planta.models import Configuracion, Payload

import datetime

@login_required
def res_registrar(request):
	razas = Raza.objects.all()
	if request.method == 'POST':
		id_arete = request.POST['id_arete']
		id_arete_siniiga = request.POST['id_arete_siniiga']
		corral = request.POST['corral']
		fecha_entrada = request.POST['fecha_entrada']
		precio_compra = request.POST['precio_compra']
		raza = request.POST['raza']
		sexo = request.POST['radioSexo']
		fecha_nacimiento = request.POST['fecha_nacimiento']
		peso = request.POST['peso']
		altura = request.POST['altura']

		r = Res()
		r.fecha_ingreso = fecha_entrada
		r.raza = get_object_or_404(Raza, pk=int(raza))
		#r.edad_ingreso = fecha_nacimiento
		r.sexo = sexo
		r.precio_compra = precio_compra
		r.arete = id_arete
		r.arete_siniiga = id_arete_siniiga
		r.save()
		messages.success(request, 'Res Agregada')


	return render(request, 'res/res_registrar.html', { 'razas': razas })

@login_required
def res_ver(request, res):
	res = get_object_or_404(Res, pk=int(res))
	config = Configuracion.objects.last()
	ctemp = Payload( config.temperatura )
	cfres = Payload( config.frespiratoria )
	cfcard = Payload( config.fcardiaca )
	return render(request, 'res/res_ver.html', {'res': res, 'ctemp': ctemp, 'cfres': cfres, 'cfcard': cfcard})

def res_alerta(request, res):
	res = get_object_or_404(Res, pk=int(res))
	if request.method == 'POST':
		conf = Configuracion.objects.last()
		estado = res.estado()
		n_fecha = estado.fecha + datetime.timedelta(minutes=conf.tiempo_email)
		
		estado.comentario = request.POST['comentarios']
		estado.estado = request.POST['estados']
		estado.fecha = n_fecha
		estado.save()
		messages.success(request, 'Alerta Modificada')
	return render(request, 'res/res_alerta.html', { 'res': res })

class RazaListView(ListView):
	model = Raza
	template_name = 'res/res_raza_ver.html'

class RazaCreateView(CreateView):
	template_name = 'res/res_raza.html'
	model = Raza
	fields = ['nombre']
	def form_valid(self, form):
		messages.success(self.request, 'Raza Creada')
		return super(RazaCreateView, self).form_valid(form)

class RazaDetailView(UpdateView):
	template_name = 'res/res_raza.html'
	model = Raza
	fields = ['nombre']
	def form_valid(self, form):
		messages.success(self.request, 'Raza Modificada')
		return super(RazaDetailView, self).form_valid(form)