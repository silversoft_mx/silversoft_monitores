# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('res', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Corral',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='medida',
            name='edad',
            field=models.CharField(max_length=5, blank=True),
        ),
        migrations.AddField(
            model_name='res',
            name='corral',
            field=models.ForeignKey(blank=True, to='res.Corral', null=True),
        ),
    ]
