# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.datetime_safe


class Migration(migrations.Migration):

    dependencies = [
        ('res', '0002_auto_20151104_1804'),
    ]

    operations = [
        migrations.AddField(
            model_name='alerta',
            name='comentario',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='alerta',
            name='updatedAt',
            field=models.DateTimeField(default=django.utils.datetime_safe.datetime.now, auto_now=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='alerta',
            name='estado',
            field=models.CharField(default=b'checar', max_length=20, choices=[(b'checar', b'CHECAR'), (b'revisado', b'CHECADO'), (b'finalizado', b'FINALIZADO')]),
        ),
    ]
