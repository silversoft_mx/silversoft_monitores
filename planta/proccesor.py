from res.models import Alerta

def informacion(request):
	alertas = Alerta.objects.exclude(estado='finalizado').order_by('-res', '-fecha')
	datos = {
		'alertas': alertas
	}
	return datos