from django.db import models
import json

class Payload(object):
	def __init__(self, j):
		self.__dict__ = json.loads(j)

class Planta(models.Model):
	code = models.CharField(max_length=200)
	nombre = models.CharField(max_length=100)
	ubicacion = models.TextField(blank=True)

	def __unicode__(self):
		return u'%s' % self.nombre

class Estado(models.Model):
	nombre = models.CharField(max_length=100)

	def __unicode__(self):
		return u'%s' % self.nombre

class Ciudad(models.Model):
	nombre = models.CharField(max_length=100)
	estado = models.ForeignKey(Estado)

	def __unicode__(self):
		return u'%s' % self.nombre

class Configuracion(models.Model):
	temperatura = models.CharField(max_length=200, blank=True, null=True, 
		default='{"normal":{"min":37,"max":38.99}, "moderada":{"min":39,"max":39.99}, "critica":{"min":36.99, "max":39.99}}')

	frespiratoria = models.CharField(max_length=200, blank=True, null=True, 
		default='{"normal":{"min":14,"max":21}, "moderada":{"min":11,"max":15}, "critica":{"min":12, "max":22}}')

	fcardiaca = models.CharField(max_length=200, blank=True, null=True, 
		default='{"normal":{"min":69,"max":81}, "moderada":{"min":64,"max":70}, "critica":{"min":65, "max":85}}')
	
	tiempo_peticion_res = models.IntegerField(blank=True, null=True, default=30)
	tiempo_peticion_res_enferma = models.IntegerField(blank=True, null=True, default=15)

	intentos_email = models.IntegerField(blank=True, null=True, default=3)
	tiempo_email = models.IntegerField(blank=True, null=True, default=3)

	def __unicode__(self):
		return u'%s' % self.pk

