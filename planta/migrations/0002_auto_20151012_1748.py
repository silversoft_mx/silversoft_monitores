# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('planta', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='res',
            name='proveedor',
            field=models.ForeignKey(blank=True, to='planta.Proveedor', null=True),
        ),
        migrations.AlterField(
            model_name='res',
            name='raza',
            field=models.ForeignKey(blank=True, to='planta.Raza', null=True),
        ),
    ]
