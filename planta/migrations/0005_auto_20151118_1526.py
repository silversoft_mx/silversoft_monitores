# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('planta', '0004_configuracion'),
    ]

    operations = [
        migrations.AddField(
            model_name='configuracion',
            name='intentos_email',
            field=models.IntegerField(default=3, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='configuracion',
            name='tiempo_email',
            field=models.IntegerField(default=3, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='configuracion',
            name='fcardiaca',
            field=models.CharField(default=b'{"normal":{"min":69,"max":81}, "moderada":[{"min":64,"max":70}, {"min":80,"max":86}], "critica":{"min":65, "max":85}}', max_length=200, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='configuracion',
            name='frespiratoria',
            field=models.CharField(default=b'{"normal":{"min":14,"max":21}, "moderada":[{"min":11,"max":15}, {"min":20,"max":23}], "critica":{"min":12, "max":22}}', max_length=200, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='configuracion',
            name='temperatura',
            field=models.CharField(default=b'{"normal":{"min":37,"max":38.99}, "moderada":[{"min":39,"max":39.99}, {"min":37,"max":37.99}], "critica":{"min":36.99, "max":39.99}}', max_length=200, null=True, blank=True),
        ),
    ]
