# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Alerta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('estado', models.CharField(default=b'CHECAR', max_length=20, choices=[(b'CHECAR', b'CHECAR'), (b'CHECADO', b'CHECADO'), (b'FINALIZADO', b'FINALIZADO')])),
                ('fecha', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Ciudad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Enfermedad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
                ('tipo', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Enfermedad_Transaccion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('problema', models.TextField()),
                ('enfermedad', models.ForeignKey(to='planta.Enfermedad')),
            ],
        ),
        migrations.CreateModel(
            name='Estado',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Medida',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('peso', models.DecimalField(max_digits=6, decimal_places=2)),
                ('altura', models.DecimalField(max_digits=6, decimal_places=2)),
                ('fecha', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Planta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=200)),
                ('nombre', models.CharField(max_length=100)),
                ('ubicacion', models.TextField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Proveedor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
                ('telefono', models.CharField(max_length=15, blank=True)),
                ('descripcion', models.TextField(blank=True)),
                ('ciudad', models.ForeignKey(to='planta.Ciudad', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Raza',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Receptor',
            fields=[
                ('id', models.IntegerField(unique=True, serialize=False, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Res',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fecha_ingreso', models.DateField(auto_now_add=True)),
                ('edad_ingreso', models.IntegerField(null=True, blank=True)),
                ('sexo', models.CharField(default=b'H', max_length=1, choices=[(b'M', b'Macho'), (b'H', b'Hembra')])),
                ('fecha_salida', models.DateField(null=True, blank=True)),
                ('muerto', models.BooleanField(default=False)),
                ('precio_compra', models.DecimalField(null=True, max_digits=6, decimal_places=2, blank=True)),
                ('precio_venta', models.DecimalField(null=True, max_digits=6, decimal_places=2, blank=True)),
                ('arete', models.IntegerField(unique=True, null=True, blank=True)),
                ('arete_siniiga', models.CharField(max_length=100, blank=True)),
                ('receptor', models.IntegerField(null=True, blank=True)),
                ('proveedor', models.ForeignKey(to='planta.Proveedor', blank=True)),
                ('raza', models.ForeignKey(to='planta.Raza', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Signo_Vital',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('temperatura', models.DecimalField(max_digits=6, decimal_places=2)),
                ('frecuencia_cardiaca', models.DecimalField(max_digits=6, decimal_places=2)),
                ('frecuencia_respiratoria', models.DecimalField(max_digits=6, decimal_places=2)),
                ('fecha', models.DateTimeField(auto_now_add=True)),
                ('res', models.ForeignKey(to='planta.Res')),
            ],
        ),
        migrations.AddField(
            model_name='medida',
            name='res',
            field=models.ForeignKey(to='planta.Res'),
        ),
        migrations.AddField(
            model_name='enfermedad_transaccion',
            name='res',
            field=models.ForeignKey(to='planta.Res'),
        ),
        migrations.AddField(
            model_name='enfermedad_transaccion',
            name='veterinario',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='ciudad',
            name='estado',
            field=models.ForeignKey(to='planta.Estado'),
        ),
        migrations.AddField(
            model_name='alerta',
            name='res',
            field=models.ForeignKey(to='planta.Res'),
        ),
    ]
