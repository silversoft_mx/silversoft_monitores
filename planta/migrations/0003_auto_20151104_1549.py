# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('planta', '0002_auto_20151012_1748'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='alerta',
            name='res',
        ),
        migrations.RemoveField(
            model_name='enfermedad_transaccion',
            name='enfermedad',
        ),
        migrations.RemoveField(
            model_name='enfermedad_transaccion',
            name='res',
        ),
        migrations.RemoveField(
            model_name='enfermedad_transaccion',
            name='veterinario',
        ),
        migrations.RemoveField(
            model_name='medida',
            name='res',
        ),
        migrations.RemoveField(
            model_name='proveedor',
            name='ciudad',
        ),
        migrations.DeleteModel(
            name='Receptor',
        ),
        migrations.RemoveField(
            model_name='res',
            name='proveedor',
        ),
        migrations.RemoveField(
            model_name='res',
            name='raza',
        ),
        migrations.RemoveField(
            model_name='signo_vital',
            name='res',
        ),
        migrations.DeleteModel(
            name='Alerta',
        ),
        migrations.DeleteModel(
            name='Enfermedad',
        ),
        migrations.DeleteModel(
            name='Enfermedad_Transaccion',
        ),
        migrations.DeleteModel(
            name='Medida',
        ),
        migrations.DeleteModel(
            name='Proveedor',
        ),
        migrations.DeleteModel(
            name='Raza',
        ),
        migrations.DeleteModel(
            name='Res',
        ),
        migrations.DeleteModel(
            name='Signo_Vital',
        ),
    ]
