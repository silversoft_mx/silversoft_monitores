# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('planta', '0003_auto_20151104_1549'),
    ]

    operations = [
        migrations.CreateModel(
            name='Configuracion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('temperatura', models.CharField(default=b'{"normal:"{"min":37,"max":38.99}, "moderada":[{"min":39,"max":39.99}], {"min":37,"max":37.99}], "critica":{"min":36.99, "max":39.99}}', max_length=200, null=True, blank=True)),
                ('frespiratoria', models.CharField(default=b'{"normal:"{"min":14,"max":21}, "moderada":[{"min":11,"max":15}], {"min":20,"max":23}], "critica":{"min":12, "max":22}}', max_length=200, null=True, blank=True)),
                ('fcardiaca', models.CharField(default=b'{"normal:"{"min":69,"max":81}, "moderada":[{"min":64,"max":70}], {"min":80,"max":86}], "critica":{"min":65, "max":85}}', max_length=200, null=True, blank=True)),
                ('tiempo_peticion_res', models.IntegerField(default=30, null=True, blank=True)),
                ('tiempo_peticion_res_enferma', models.IntegerField(default=15, null=True, blank=True)),
            ],
        ),
    ]
