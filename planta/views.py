# coding=utf-8
from django.shortcuts import render, redirect
from django.contrib.auth import logout, login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from res.models import Res
from .models import Configuracion, Payload

import json

def home(request):	
	return redirect('/inicio')

def entrar(request):
	if request.user.is_authenticated():
		return redirect('/')

	if request.method == 'POST':
		email = request.POST['email']
		password = request.POST['password']
		
		user = authenticate( username=email, password=password )
		if user is not None:
			login(request, user)
			return redirect('/')
		else:			
			messages.error(request, 'Usuario/Contraseña incorrecta')
	return render(request, 'planta/entrar.html')


def salir(request):
	logout(request)
	return redirect('/')

@login_required
def inicio(request):
	reses = Res.objects.all()
	config = Configuracion.objects.last()
	ctemp = Payload( config.temperatura )
	cfres = Payload( config.frespiratoria )
	cfcard = Payload( config.fcardiaca )

	return render(request, 'planta/inicio.html', { 'reses': reses, 'ctemp': ctemp, 'cfres': cfres, 'cfcard': cfcard })

@login_required
def configuracion(request):
	configuracion_est = Configuracion.objects.last()
	

	if request.method == 'POST':
		conf = json.loads( configuracion_est.temperatura )
		conf['normal']['min'] = request.POST['tmp_n_min']
		conf['normal']['max'] = request.POST['tmp_n_max']
		conf['moderada']['min'] = request.POST['tmp_m_min']
		conf['moderada']['max'] = request.POST['tmp_m_max']
		configuracion_est.temperatura = json.dumps( conf )

		conf = json.loads( configuracion_est.frespiratoria )
		conf['normal']['min'] = request.POST['fres_n_min']
		conf['normal']['max'] = request.POST['fres_n_max']
		conf['moderada']['min'] = request.POST['fres_m_min']
		conf['moderada']['max'] = request.POST['fres_m_max']
		configuracion_est.frespiratoria = json.dumps( conf )

		conf = json.loads( configuracion_est.fcardiaca )
		conf['normal']['min'] = request.POST['fcar_n_min']
		conf['normal']['max'] = request.POST['fcar_n_max']
		conf['moderada']['min'] = request.POST['fcar_m_min']
		conf['moderada']['max'] = request.POST['fcar_m_max']
		configuracion_est.fcardiaca = json.dumps( conf )

		configuracion_est.save()

	temperatura = Payload(configuracion_est.temperatura)
	temperatura_normal_max = temperatura.normal['max']
	temperatura_normal_min = temperatura.normal['min']
	temperatura_moderada_max = temperatura.moderada['max']
	temperatura_moderada_min = temperatura.moderada['min']

	frespiratoria = Payload(configuracion_est.frespiratoria)
	frespiratoria_normal_max = frespiratoria.normal['max']
	frespiratoria_normal_min = frespiratoria.normal['min']
	frespiratoria_moderada_max = frespiratoria.moderada['max']
	frespiratoria_moderada_min = frespiratoria.moderada['min']

	fcardiaca = Payload(configuracion_est.fcardiaca)
	fcardiaca_normal_max = fcardiaca.normal['max']
	fcardiaca_normal_min = fcardiaca.normal['min']
	fcardiaca_moderada_max = fcardiaca.moderada['max']
	fcardiaca_moderada_min = fcardiaca.moderada['min']

	data = {
		'configuracion_est': configuracion_est,
		'temperatura_normal_max' : temperatura_normal_max,
		'temperatura_normal_min' : temperatura_normal_min,
		'temperatura_moderada_max' : temperatura_moderada_max,
		'temperatura_moderada_min' : temperatura_moderada_min,
		'frespiratoria_normal_max' : frespiratoria_normal_max,
		'frespiratoria_normal_min' : frespiratoria_normal_min,
		'frespiratoria_moderada_max' : frespiratoria_moderada_max,
		'frespiratoria_moderada_min' : frespiratoria_moderada_min,
		'fcardiaca_normal_max' : fcardiaca_normal_max,
		'fcardiaca_normal_min' : fcardiaca_normal_min,
		'fcardiaca_moderada_max' : fcardiaca_moderada_max,
		'fcardiaca_moderada_min' : fcardiaca_moderada_min,
	}

	return render(request, 'planta/configuracion.html', data)
